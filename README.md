# JJSearchViewAnim


#### 项目介绍

- 项目名称：JJSearchViewAnim
- 所属系列：openharmony第三方组件适配移植
- 功能：一个炫酷的SearchView搜索动画库,支持8中不同的动画。
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本：master分支



#### 效果演示
<p align="center">
  <img src="image/demo.gif"   />
</p>




#### 安装教程

项目需要添加maven仓库，并且再build.gradle文件下添加依赖

 ```gradle
// 添加maven仓库
repositories {
    maven {
        url 'https://s01.oss.sonatype.org/content/repositories/releases/'
    }
}

// 添加依赖库
dependencies {

implementation('com.gitee.chinasoft_ohos:JJSearchViewAnim:1.0.0')
    ......
}

 ```
在sdk6，DevEco Studio 2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明
(1) 在布局文件xml中

```xml
  <com.cjj.sva.JJSearchView
         ohos:id="$+id:SearchView"
         ohos:height="match_parent"
         ohos:width="match_parent"/>
```
(2) 在java代码中 
```java
  @Override 
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        mJJSearchView = (JJSearchView) findComponentById(ResourceTable.Id_SearchView);
        mJJSearchView.setController(new JJChangeArrowController());}
```
(3) 设置动画开启及恢复
```java
mJJSearchView.startAnim();
mJJSearchView.resetAnim();
```

#### 测试信息
CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异


#### 版本迭代

1.0.0

#### 版权和许可信息
```
The MIT License (MIT)

Copyright (c) 2016 android-cjj

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```