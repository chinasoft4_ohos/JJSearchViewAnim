package com.cjj.jjsearchviewanim;

import com.cjj.sva.JJSearchView;
import com.cjj.sva.anim.controller.*;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.PopupDialog;
import ohos.agp.window.service.DisplayManager;
import ohos.agp.window.service.WindowManager;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

public class MainAbility extends Ability implements Component.ClickedListener {
    private JJSearchView mJJSearchView;
    private static String action = "";
    private  MenuDialog inputDialog;
    private  Component textSetting;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(new Color(Color.getIntColor("#303F9F")).getValue());
        super.setUIContent(ResourceTable.Layout_ability_main);
        mJJSearchView = (JJSearchView) findComponentById(ResourceTable.Id_SearchView);
        mJJSearchView.setController(new JJChangeArrowController());
        Button textStart = (Button) findComponentById(ResourceTable.Id_start);
        Button textReset = (Button) findComponentById(ResourceTable.Id_reset);
        textSetting = findComponentById(ResourceTable.Id_setting);
        textReset.setClickedListener(this);
        textStart.setClickedListener(this);
        textSetting.setClickedListener(this);

    }

    private void start() {
        mJJSearchView.startAnim();

    }

    private void reset() {
        mJJSearchView.resetAnim();
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_start:
                start();
                break;
            case ResourceTable.Id_reset:
                reset();
                break;
            case ResourceTable.Id_setting:
                showMenu();
                break;
        }
    }

    private void showMenu() {
        inputDialog = new MenuDialog(getContext(), textSetting);
        inputDialog.setDialogListener(() -> {
            inputDialog.destroy();
            return false;
        });
        inputDialog.show();
        inputDialog.setOnClickListener(() -> {
            inputDialog.hide();
            inputDialog.destroy();
            switch (action) {
                case "action1":
                    mJJSearchView.setController(new JJAroundCircleBornTailController());
                    break;
                case "action2":
                    mJJSearchView.setController(new JJBarWithErrorIconController());
                    break;
                case "action3":
                    mJJSearchView.setController(new JJChangeArrowController());
                    break;
                case "action4":
                    mJJSearchView.setController(new JJCircleToBarController());
                    break;
                case "action5":
                    mJJSearchView.setController(new JJCircleToLineAlphaController());
                    break;
                case "action6":
                    mJJSearchView.setController(new JJCircleToSimpleLineController());
                    break;
                case "action7":
                    mJJSearchView.setController(new JJDotGoPathController());
                    break;
                case "action8":
                    mJJSearchView.setController(new JJScaleCircleAndTailController());
                    break;
            }
        });

    }

    private static class MenuDialog extends PopupDialog {
        private final ArrayList<String> list = new ArrayList<>();
        private MenuDialog.onClickListener onClickListener;

        public MenuDialog(Context context, Component contentComponent) {
            super(context, contentComponent);
            init(context);
            int width = DisplayManager.getInstance().getDefaultDisplay(context).get().getAttributes().width;
            int dialogWidth = AttrHelper.vp2px(260f, context);
            int dialogHeight = AttrHelper.vp2px(300f, context);
            setSize(dialogWidth, dialogHeight);
            setCornerRadius(10f);
            int offWidth = width - dialogWidth;
            setOffset(offWidth - AttrHelper.vp2px(5f, context), AttrHelper.vp2px(56f, context));
        }


        public void setOnClickListener(MenuDialog.onClickListener onClickListener) {
            this.onClickListener = onClickListener;
        }

        private void init(Context context) {
            list.add("JJDotGoPathController");
            list.add("JJAroundCircleBornTailController");
            list.add("JJBarWithErrorIconController");
            list.add("JJScaleCircleAndTailController");
            list.add("JJChangeArrowController");
            list.add("JJCircleToLineAlphaController");
            list.add("JJCircleToBarController");
            list.add("JJCircleToSimpleLineController");
            this.setTransparent(false);
            MenuDialog.Menu inputView = new MenuDialog.Menu(context);
            setCustomComponent(inputView);
            ListContainer listContainer1 = (ListContainer) inputView.findComponentById(ResourceTable.Id_list);
            MenuDialog.MenuProvider menuProvider = new MenuDialog.MenuProvider(list, context);
            listContainer1.setLongClickable(false);
            listContainer1.setItemProvider(menuProvider);
            listContainer1.setItemClickedListener((listContainer, component, i, l) -> {
                switch (i) {
                    case 0:
                        action = "action7";
                        break;

                    case 1:
                        action = "action1";
                        break;
                    case 2:
                        action = "action2";
                        break;

                    case 3:
                        action = "action8";
                        break;
                    case 4:
                        action = "action3";
                        break;
                    case 5:
                        action = "action5";
                        break;
                    case 6:
                        action = "action4";
                        break;
                    case 7:
                        action = "action6";
                        break;

                }
                onClickListener.onItemClick();
            });
        }

        public interface onClickListener {
            void onItemClick();
        }

        /**
         * Menu
         *
         * @author wz
         * @since 2021-06-28
         */
        private  static class Menu extends DirectionalLayout {
            Menu(Context context) {
                super(context);
                setupComponent(context);
            }

            private void setupComponent(Context context) {
                LayoutScatter.getInstance(context).parse(ResourceTable.Layout_menu, this, true);

            }
        }

        private static class MenuProvider extends BaseItemProvider {
            private final List<String> list;
            private final Context context;

            MenuProvider(List<String> list, Context context) {

                this.context = context;
                this.list = list;
            }

            @Override
            public int getCount() {
                return list.size();
            }

            @Override
            public Object getItem(int position) {
                if (list != null && position >= 0 && position < list.size()) {
                    return list.get(position);
                }
                return new Object();
            }

            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public Component getComponent(int position, Component convertComponent, ComponentContainer componentContainer) {
                final Component cpt;
                cpt = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_item, null, false);
                String str = list.get(position);
                Text text = (Text) cpt.findComponentById(ResourceTable.Id_text);

                text.setText(str);
                return cpt;
            }
        }
    }
}
