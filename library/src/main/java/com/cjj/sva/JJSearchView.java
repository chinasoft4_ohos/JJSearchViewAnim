package com.cjj.sva;


import com.cjj.sva.anim.JJBaseController;
import com.cjj.sva.anim.controller.JJChangeArrowController;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.app.Context;

/**
 * 这是一个神奇的类，今天心情郁闷
 */
public class JJSearchView extends Component implements Component.DrawTask {
    private Paint mPaint;
    private JJBaseController mController = new JJChangeArrowController();

    public JJSearchView(Context context) {
        this(context, null);
    }

    public JJSearchView(Context context, AttrSet attrs) {
        this(context, attrs, "");
    }

    public JJSearchView(Context context, AttrSet attrs, String  defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mPaint=new Paint();
        mPaint.setStrokeWidth(4);
        this.addDrawTask(this::onDraw);
    }

    public void setController(JJBaseController controller) {
        this.mController = controller;
        mController.setSearchView(this);
        invalidate();
    }

    public void startAnim() {
        if (mController != null)
            mController.startAnim();
    }

    public void resetAnim() {
        if (mController != null)
            mController.resetAnim();
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        mController.draw(canvas, mPaint);
    }

}
