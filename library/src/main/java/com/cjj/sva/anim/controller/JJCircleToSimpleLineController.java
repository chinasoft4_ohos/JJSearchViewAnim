package com.cjj.sva.anim.controller;

import com.cjj.sva.anim.JJBaseController;
import ohos.agp.render.Arc;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;

/**
 * 这是一个神奇的类，SearchView变成一条线，然而并没有什么卵用！
 */
public class JJCircleToSimpleLineController extends JJBaseController {
    private String mColor = "#4CAF50";
    private int cx, cy, cr;
    private RectFloat mRectF;
    private int jPadding = 10;
    private Paint bgPaint;

    public JJCircleToSimpleLineController() {
        mRectF = new RectFloat();
        bgPaint = new Paint();
        bgPaint.setColor(new Color(Color.getIntColor(mColor)));
    }

    @Override
    public void draw(Canvas canvas, Paint paint) {
        canvas.drawRect(0,0,getWidth(),getHeight(),bgPaint);
        switch (mState) {
            case STATE_ANIM_NONE:
                drawNormalView(paint, canvas);
                break;
            case STATE_ANIM_START:
                drawStartAnimView(paint, canvas);
                break;
            case STATE_ANIM_STOP:
                drawStopAnimView(paint, canvas);
                break;
        }
    }

    private void drawStopAnimView(Paint paint, Canvas canvas) { //mPro >= 0.2f ? mPro : 0.2f
        canvas.save();
        canvas.drawLine(multiply(sub(add(mRectF.right , cr) , jPadding) , (Math.max(mPro, 0.2f))),
                add(mRectF.bottom , cr, -jPadding), (mRectF.right + cr - jPadding), mRectF.bottom + cr - jPadding, paint);
        if (mPro > 0.5f) {
            canvas.drawArc(mRectF,new Arc( 45, -((mPro - 0.5f) * 360 * 2),false),paint);
            canvas.drawLine(mRectF.right - jPadding, mRectF.bottom - jPadding,
                    mRectF.right + cr - jPadding, mRectF.bottom + cr - jPadding, paint);
        } else {
            canvas.drawLine(mRectF.right - jPadding + cr * (1 - mPro), mRectF.bottom - jPadding +
                    cr * (1 - mPro), mRectF.right + cr - jPadding, mRectF.bottom + cr - jPadding, paint);
        }
        canvas.restore();
        mRectF.left = cx - cr + 240 * (1 - mPro);
        mRectF.right = cx + cr + 240 * (1 - mPro);
        mRectF.top = cy - cr;
        mRectF.bottom = cy + cr;
    }

    private void drawStartAnimView(Paint paint, Canvas canvas) {
        canvas.save();
        if (mPro <= 0.5f) {
            canvas.drawArc(mRectF,new Arc( 45, -(360 - 360 * 2 * (mPro == -1 ? 1 : mPro)),false),paint);
            canvas.drawLine(mRectF.right - jPadding, mRectF.bottom - jPadding,
                    mRectF.right + cr - jPadding, mRectF.bottom + cr - jPadding, paint);
        } else {
            canvas.drawLine(mRectF.right - jPadding + cr * mPro, mRectF.bottom - jPadding + cr * mPro,
                    mRectF.right + cr - jPadding, mRectF.bottom + cr - jPadding, paint);
        }

        canvas.drawLine((mRectF.right + cr - jPadding) * (1 - mPro * .8f), mRectF.bottom + cr - jPadding,
                mRectF.right + cr - jPadding, mRectF.bottom + cr - jPadding, paint);
        canvas.restore();

        mRectF.left = cx - cr + 240 * mPro;
        mRectF.right = cx + cr + 240 * mPro;
        mRectF.top = cy - cr;
        mRectF.bottom = cy + cr;
    }

    private void drawNormalView(Paint paint, Canvas canvas) {
        cr = getWidth() / 24;
        cx = getWidth() / 2;
        cy = getHeight() / 2;
        mRectF.left = cx - cr;
        mRectF.right = cx + cr;
        mRectF.top = cy - cr;
        mRectF.bottom = cy + cr;

        canvas.save();

        paint.reset();
        paint.setAntiAlias(true);
        paint.setColor(Color.WHITE);
        paint.setStrokeWidth(4);
        paint.setStyle(Paint.Style.STROKE_STYLE);

        canvas.rotate(45, cx, cy);
        canvas.drawLine(cx + cr, cy, cx + cr * 2, cy, paint);
        canvas.drawArc(mRectF,new Arc(  0, 360,false),paint);
        canvas.restore();
    }

    @Override
    public void startAnim() {
        if (mState == STATE_ANIM_START) return;
        mState = STATE_ANIM_START;
        startSearchViewAnim();
    }

    @Override
    public void resetAnim() {
        if (mState == STATE_ANIM_STOP) return;
        mState = STATE_ANIM_STOP;
        startSearchViewAnim();
    }

}
