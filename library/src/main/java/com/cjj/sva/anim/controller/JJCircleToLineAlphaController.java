package com.cjj.sva.anim.controller;

import com.cjj.sva.anim.JJBaseController;
import ohos.agp.render.Arc;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;

/**
 * 这是一个神奇的类，searchview释放外面的圈圈，呼吸新鲜空气！
 */
public class JJCircleToLineAlphaController extends JJBaseController {
    private String mColor = "#673AB7";
    private int cx, cy, cr;
    private RectFloat mRectF, mRectF2;
    private float sign = 0.707f;
    private float tran = 120;
    private Paint bgPaint;

    public JJCircleToLineAlphaController() {
        mRectF = new RectFloat();
        mRectF2 = new RectFloat();
        bgPaint = new Paint();
        bgPaint.setColor(new Color(Color.getIntColor(mColor)));
    }

    @Override
    public void draw(Canvas canvas, Paint paint) {
        canvas.drawRect(0, 0, getWidth(), getHeight(), bgPaint);
        switch (mState) {
            case STATE_ANIM_NONE:
                drawNormalView(paint, canvas);
                break;
            case STATE_ANIM_START:
                drawStartAnimView(paint, canvas);
                break;
            case STATE_ANIM_STOP:
                drawStopAnimView(paint, canvas);
                break;
        }
    }

    private void drawStopAnimView(Paint paint, Canvas canvas) {
        canvas.save();
        if (mPro > 0.7) {
            paint.setAlpha((int) (mPro * 255));
            drawNormalView(paint, canvas);
        }
        canvas.restore();
    }

    private void drawStartAnimView(Paint paint, Canvas canvas) {
        canvas.save();
        canvas.drawLine(add(mRectF.left,cr,multiply(cr , sign)), add(mRectF.top ,cr,multiply(cr , sign)),
                add(mRectF.left,cr,multiply(2 , cr , sign)), add(mRectF.top,cr,multiply(2 , cr , sign)), paint);
        canvas.drawArc(mRectF, new Arc(0, 360, false), paint);
        canvas.drawArc(mRectF2, new Arc(90, multiply(-360 , sub(1 , mPro)), false), paint);
        if (mPro >= 0.7f) {
            canvas.drawLine(multiply(add(sub(1 , mPro) , 0.7f),sub(mRectF2.right , multiply(3 ,cr))), mRectF2.bottom,
                    sub(mRectF2.right ,multiply(3 ,cr)), mRectF2.bottom, paint);
        }
        canvas.restore();
        mRectF.left = add(sub(cx, cr), multiply(tran, mPro));
        mRectF.right =add(cx,cr,multiply(tran, mPro));
        mRectF2.left =add(sub(cx ,multiply(3, cr) ), multiply(tran, mPro));
        mRectF2.right = add(cx, multiply(3, cr), multiply(tran, mPro));
    }

    private void drawNormalView(Paint paint, Canvas canvas) {
        cr = getWidth() / 50;
        cx = getWidth() / 2;
        cy = getHeight() / 2;
        mRectF.left =sub(cx, cr) ;
        mRectF.right = add(cx, cr);
        mRectF.top = sub(cy ,cr);
        mRectF.bottom = add(cy , cr);

        mRectF2.left = sub(cx , multiply(3, cr));
        mRectF2.right =add(cx , multiply(3, cr));
        mRectF2.top = sub(cy , multiply(3, cr));
        mRectF2.bottom = add(cy , multiply(3, cr));

        canvas.save();
        paint.reset();
        paint.setAntiAlias(true);
        paint.setColor(Color.WHITE);
        paint.setStrokeWidth(4);
        paint.setStyle(Paint.Style.STROKE_STYLE);

        canvas.rotate(45, cx, cy);
        canvas.drawLine(cx + cr, cy, cx + cr * 2, cy, paint);
        canvas.drawArc(mRectF, new Arc(0, 360, false), paint);
        canvas.drawArc(mRectF2, new Arc(0, 360, false), paint);
        canvas.restore();
    }

    @Override
    public void startAnim() {
        if (mState == STATE_ANIM_START) return;
        mState = STATE_ANIM_START;
        startSearchViewAnim();
    }

    @Override
    public void resetAnim() {
        if (mState == STATE_ANIM_STOP) return;
        mState = STATE_ANIM_STOP;
        startSearchViewAnim();
    }
}
