package com.cjj.sva.anim.controller;


import com.cjj.sva.anim.JJBaseController;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;

/**
 * 这是一个神奇的类，SearchView可以放大缩小，别私用哦！
 */
public class JJScaleCircleAndTailController extends JJBaseController {
    private String mColor = "#2196F3";
    private int cx, cy, cr, scr;
    private Paint bgPaint;
    @Override
    public void draw(Canvas canvas, Paint paint) {
        switch (mState) {
            case STATE_ANIM_NONE:
                drawNormalView(paint, canvas);
                break;
            case STATE_ANIM_START:
                drawStartAnimView(paint, canvas);
                break;
            case STATE_ANIM_STOP:
                drawStopAnimView(paint, canvas);
                break;
        }
    }

    private void drawStopAnimView(Paint paint, Canvas canvas) {
        drawNormalView(paint, canvas);
        drawSearchView(paint, canvas, false);
    }

    private void drawStartAnimView(Paint paint, Canvas canvas) {
        drawNormalView(paint, canvas);
        drawSearchView(paint, canvas, true);
    }

    private void drawSearchView(Paint paint, Canvas canvas, boolean start) {
        drawNormalView(paint, canvas);
        paint.setStyle(Paint.Style.FILL_STYLE);
        canvas.drawCircle(cx, cy, multiply((sub(cr,25)) ,(start ?
                mPro : sub(1 , mPro))), paint);
        canvas.rotate(130, cx, cy);

        canvas.save();
        paint.setColor(Color.WHITE);
        paint.setStrokeWidth(25);
        float sx=add(add(cx,div(cr,2),20),multiply(div(cr,2) , (start ? mPro : sub(1 , mPro))));

        float ex=add(cx,cr, multiply(div(getWidth(),5) ,
                (start ? mPro :  sub(1 , mPro))));
        canvas.drawLine(sx, cy, ex, cy, paint);
        canvas.restore();
    }

    private void drawNormalView(Paint paint, Canvas canvas) {
        cr = getWidth() / 6;
        cx = getWidth() / 2;
        cy = getHeight() / 2;
        scr = getWidth() / 18;
        bgPaint = new Paint();
        bgPaint.setColor(new Color(Color.getIntColor(mColor)));
        canvas.drawRect(0,0,getWidth(),getHeight(),bgPaint);
        paint.reset();
        paint.setAntiAlias(true);
        paint.setColor(Color.WHITE);
        canvas.drawCircle(cx, cy, cr, paint);

        canvas.save();
        paint.setColor(new Color(Color.getIntColor(mColor)));
        paint.setStrokeWidth(10);
        paint.setStyle(Paint.Style.STROKE_STYLE);
        canvas.rotate(130, cx, cy);
        canvas.drawLine(cx + scr + 10, cy, cx + scr * 2, cy, paint);
        canvas.drawCircle(cx, cy, scr, paint);
        canvas.restore();
    }

    @Override
    public void startAnim() {
        if (mState == STATE_ANIM_START) return;
        mState = STATE_ANIM_START;
        startSearchViewAnim();
    }

    @Override
    public void resetAnim() {
        if (mState == STATE_ANIM_STOP) return;
        mState = STATE_ANIM_STOP;
        startSearchViewAnim();
    }

}
