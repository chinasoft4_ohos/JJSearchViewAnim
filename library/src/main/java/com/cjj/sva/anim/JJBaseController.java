package com.cjj.sva.anim;

import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PathMeasure;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.ref.WeakReference;
import java.math.BigDecimal;

/**
 * 这是一个神奇的controller，神奇你妹啊...妹啊...啊。
 */
public abstract class JJBaseController {
    public static final int STATE_ANIM_NONE = 0;
    public static final int STATE_ANIM_START = 1;
    public static final int STATE_ANIM_STOP = 2;
    public static final int DEFAULT_ANIM_TIME = 500;
    public static final float DEFAULT_ANIM_STARTF = 0;
    public static final float DEFAULT_ANIM_ENDF = 1;

    protected float mPro = -1;
    private WeakReference<Component> mSearchView;
    protected float[] mPos = new float[2];

    @Retention(RetentionPolicy.SOURCE)
    public @interface State {
    }

    @State
    protected int mState = STATE_ANIM_NONE;

    public int getState() {
        return mState;
    }

    public void setState(int state) {
        mState = state;
    }

    public void setSearchView(Component searchView) {
        this.mSearchView = new WeakReference<>(searchView);
    }

    public Component getSearchView() {
        return mSearchView != null ? mSearchView.get() : null;
    }

    /**
     * 获取view的宽度
     *
     * @return Width
     */
    public int getWidth() {
        return getSearchView() != null ? getSearchView().getWidth() : 0;
    }

    /**
     * 获取view的高度
     *
     * @return Height
     */
    public int getHeight() {
        return getSearchView() != null ? getSearchView().getHeight() : 0;
    }

    /**
     * 绘制就交给我儿子们去做吧
     *
     * @param canvas canvas
     * @param paint paint
     */
    public abstract void draw(Canvas canvas, Paint paint);

    /**
     * 开启搜索动画
     */
    public void startAnim() {
    }

    /**
     * 重置搜索动画
     */
    public void resetAnim() {
    }


    public AnimatorValue startSearchViewAnim() {
        AnimatorValue valueAnimator = startSearchViewAnim(DEFAULT_ANIM_STARTF, DEFAULT_ANIM_ENDF,
                DEFAULT_ANIM_TIME);
        return valueAnimator;
    }

    public AnimatorValue startSearchViewAnim(float startF, float endF, long time) {
        AnimatorValue valueAnimator = startSearchViewAnim(startF, endF, time, null);
        return valueAnimator;
    }

    public AnimatorValue startSearchViewAnim(float startF, float endF, long time, final PathMeasure pathMeasure) {
        AnimatorValue valueAnimator = new AnimatorValue();
        valueAnimator.setDuration(time);
        valueAnimator.setValueUpdateListener((animatorValue, v) -> {
            mPro = startF + v * (endF - startF);
            if (null != pathMeasure)
                pathMeasure.getPosTan(mPro, mPos, new float[2]);
            getSearchView().invalidate();
        });

        if (!valueAnimator.isRunning()) {
            valueAnimator.start();
        }
        mPro = 0;
        return valueAnimator;
    }

    protected float add(float a, float b) {
        BigDecimal num1 = new BigDecimal(a);
        BigDecimal num2 = new BigDecimal(b);
        BigDecimal num3 = num1.add(num2);
        return num3.floatValue();
    }
    protected float add(float a, float b, float c) {
        BigDecimal num1 = new BigDecimal(a);
        BigDecimal num2 = new BigDecimal(b);
        BigDecimal num4 = new BigDecimal(c);
        BigDecimal num3 = num1.add(num2).add(num4);
        return num3.floatValue();
    }

    protected float sub(float a, float b) {
        BigDecimal num1 = new BigDecimal(a);
        BigDecimal num2 = new BigDecimal(b);
        BigDecimal num3 = num1.subtract(num2);
        return num3.floatValue();
    }

    protected float multiply(float a, float b) {
        BigDecimal num1 = new BigDecimal(a);
        BigDecimal num2 = new BigDecimal(b);
        BigDecimal num3 = num1.multiply(num2);
        return num3.floatValue();
    }

    protected float multiply(float a, float b,float c) {
        BigDecimal num1 = new BigDecimal(a);
        BigDecimal num2 = new BigDecimal(b);
        BigDecimal num4 = new BigDecimal(c);
        BigDecimal num3 = num1.multiply(num2).multiply(num4);
        return num3.floatValue();
    }
    protected float div(float a, float b) {
        BigDecimal num1 = new BigDecimal(a);
        BigDecimal num2 = new BigDecimal(b);
        BigDecimal num3 = num1.divide(num2);
        return num3.floatValue();
    }

}
