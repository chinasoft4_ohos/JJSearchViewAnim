
package com.cjj.sva.utils;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * 这是一个神奇的类，神奇就是它可以打log,而你，不可以打我！
 *
 */
public final class LogHelper {
    public static final boolean mIsDebugMode = true;
    private static final String TAG = "JJSearchView";

    private static final String CLASS_METHOD_LINE_FORMAT = "%s.%s()_%s";
    private static final HiLogLabel LABEL_LOG = new HiLogLabel(0, 0, LogHelper.TAG);

    private static final String LOG_FORMAT = "%{public}s: %{public}s";
    public static void trace(String str) {
        if (mIsDebugMode) {
            StackTraceElement traceElement = Thread.currentThread()
                    .getStackTrace()[3];
            String className = traceElement.getClassName();
            className = className.substring(className.lastIndexOf(".") + 1);
            String logText = String.format(CLASS_METHOD_LINE_FORMAT,
                    className,
                    traceElement.getMethodName(),
                    String.valueOf(traceElement.getLineNumber()));
            HiLog.error(LABEL_LOG, LOG_FORMAT, logText,"->"+ str);

        }
    }

    public static void printStackTrace(Throwable throwable) {
        if (mIsDebugMode) {
            HiLog.error(LABEL_LOG, LOG_FORMAT, "","->"+ throwable);
        }
    }

}
